(function() {

	var buildShownList = function(listId) {
		var codes = $('#' + listId + ' option').map(function() {
			return this.value;
		}).get();
		$("[data-role='serialized-list']").each(function() {
			if ($(this).data('target') === listId) {
				$(this).val(codes);
			}
		});
	}

	var isFirstSelected = function(listId) {
		var selected = false;
		$('#' + listId + ' option:selected').each(function() {
			if ($(this).index() == 0) {
				selected = true;
			}
		});
		return selected;
	}

	var isLastSelected = function(listId) {
		var selected = false;
		var lastIndex = $('#' + listId + ' option').length - 1;
		$('#' + listId + ' option:selected').each(function() {
			if ($(this).index() == lastIndex) {
				selected = true;
			}
		});
		return selected;
	}

	var isNothingSelected = function(listId) {
		return $('#' + listId + ' option:selected').length == 0;
	}

	$("[data-action='up']").click(function() {
		var listId = $(this).data('target');
		var selectedOptions = $('#' + listId + ' option:selected');
		selectedOptions.each(function() {
			$(this).prev().before($(this));
		});
		buildShownList(listId);
		toggleAllButtons();
	});

	$("[data-action='down']").click(function() {
		var listId = $(this).data('target');
		var selectedOptions = $('#' + listId + ' option:selected');
		$(selectedOptions.get().reverse()).each(function() {
			$(this).next().after($(this));
		});
		buildShownList(listId);
		toggleAllButtons();
	});

	$("[data-action='delete']").click(function() {
		var listId = $(this).data('target');
		var selectedOptions = $('#' + listId + ' option:selected');
		$.each(selectedOptions, function(i, item) {
			$(item).remove();
		});
		buildShownList(listId);
		toggleAllButtons();
	});

	var toggleUpButton = function() {
		$("[data-action='up']").each(function() {
			var listId = $(this).data('target');
			if (isFirstSelected(listId) || isNothingSelected(listId)) {
				$(this).attr('disabled', 'disabled');
			} else {
				$(this).removeAttr('disabled');
			}
		});
	}

	var toggleDownButton = function() {
		$("[data-action='down']").each(function() {
			var listId = $(this).data('target');
			if (isLastSelected(listId) || isNothingSelected(listId)) {
				$(this).attr('disabled', 'disabled');
			} else {
				$(this).removeAttr('disabled');
			}
		});
	}

	var toggleDeleteButton = function() {
		$("[data-action='delete']").each(function() {
			var listId = $(this).data('target');
			if (isNothingSelected(listId)) {
				$(this).attr('disabled', 'disabled');
			} else {
				$(this).removeAttr('disabled');
			}
		});
	}

	var toggleAllButtons = function() {
		toggleUpButton();
		toggleDownButton();
		toggleDeleteButton();
	}

	$('select').change(function() {
		toggleAllButtons();
	});

	// Attention, this was found not to be working with jQuery 3.3.1!
	$('option').livequery(function() {
		// given the last option was selected
		// (and so the 'down' button was disabled)
		// when an option is appended
		// then we want to enable the 'down' button
		// and we want to also append the new value to the serialized list
		var listId = $(this).closest('select').attr('id');
		buildShownList(listId);
		toggleAllButtons();
	});

	$(document).ready(function() {
		$("[data-role='serialized-list']").each(function() {
			var listId = $(this).data('target');
			buildShownList(listId);
		});
		toggleAllButtons();
	});
})();
