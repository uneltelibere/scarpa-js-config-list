# scarpa-js-config-list

Functionality for reorganizing `<option>`s in a `<select>`, based on jQuery.

## Features

* Move selected `option`s up or down
* Remove selected `option`s
* Automatically enable or disable the "move up", "move down" and "delete" buttons depending on the possibility based on the current selection.
* Maintain a serialized list of `option` `value` attributes, in a DOM field of your choice

## Usage

* Have a `<select>` element with an `id` and a `size` greater than `1`.
* A button for moving up needs the following attributes:
    * `data-action="up"`
    * `data-target="your-select-id"`
* A button for moving down needs the following attributes:
    * `data-action="down"`
    * `data-target="your-select-id"`
* A button for deleting needs the following attributes:
    * `data-action="delete"`
    * `data-target="your-select-id"`
* An input field (you probably want it `hidden`) for keeping the `value` list needs the following attributes:
    * `data-role="serialized-list"`
    * `data-target="your-select-id"`

## Example

```
<select id="testList" size="5" multiple="multiple">
	<option value="1">One</option>
	<option value="2">Two</option>
	<option value="3">Three</option>
</select>
<button type="button" data-action="up" data-target="testList">Up</button>
<button type="button" data-action="down" data-target="testList">Down</button>
<button type="button" data-action="delete" data-target="testList">Delete</button>
<input type="text" disabled="disabled" data-role="serialized-list"
	data-target="testList" />

<script src="webjars/jquery/1.11.2/jquery.min.js"></script>
<script src="jquery.livequery/1.3.6/jquery.livequery.min.js"></script>

<script src="scarpa-js-config-list.js"></script>
```

# License

scarpa-js-config-list is dual licensed _(just like jQuery)_ under the MIT (MIT\_LICENSE.txt) and GPL Version 2 (GPL\_LICENSE.txt) licenses.

Copyright (c) 2010 [Unelte Libere SRL](https://uneltelibere.eu/)
